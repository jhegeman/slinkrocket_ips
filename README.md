# SLINKRocket_IPs

This repository contains the IPs for the CMS community to implement the SLINKRocket link to the CMS DAQ. To use these IP the user can check out this repository and add a repository path in Vivado:
You should choose one of the subdirectories "virtex" or "kintex" or "kintexU" as repository path depending on the FPGA 
family you are using in your project. The IPs should then be visible und er the path "User Repository/CERN_CMS_DAQ" in the 
IP browser.
**NOTE**: 
- directory *kintexUP* contains the IPs for the Kintex Ultrascale Plus architecture
- directory *virtexUP* contains the IPs for the Virtex Ultrascale Plus architecture

The IPs are provided as zip files. They need to be unzipped for Vivado to find the IP inside. 

If follows a short description of the provided IPs.

## SlinkSender_IP
The SlinkSender_IP contains the firmware to send data from the FED to the DAQ over the slink using either 16Gbps or 25Gbps links. Two flavours of the IP exist, one for the kintexup series and one for the virtexup series. The kintexup
flavour offers the possibility to implement the link with either GTY or GTH transceivers. The virtex series only contains GTY transceivers. If in the kintex the GTH transceiver is chosen the user MUST choose the lower link speed of 16.366 Mbps.

## SlinkReceiver_IP
The SlinkReceiver_IP is only used by the central DAQ group (in the production system). However, it might be useful for users to use this IP in order to implement a receiver (e.g. with a test board) to test the SlinkSender of their FED. Also this IP exists in two flavours for the two Xilink architectures kintexup and virtexup. 
